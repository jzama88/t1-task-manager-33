package com.t1.alieva.tm.api.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.io.IOException;

public interface IDomainService {

    void loadDataBackup();

    void saveDataBackup();

    void loadDataBase64(@NotNull final String filename);

    void saveDataBase64(@NotNull final String filename) throws IOException;

    void loadDataBinary() throws IOException, ClassNotFoundException;

    void saveDataBinary() throws IOException;

    void loadDataJsonFasterXml() throws IOException;

    void loadDataJsonJaxB() throws JAXBException;

    void saveDataJsonFasterXml() throws IOException;

    void saveDataJsonJaxB() throws IOException, JAXBException;

    void loadDataXmlFasterXml() throws IOException;

    void loadDataXmlJaxB() throws JAXBException;

    void saveDataXmlFasterXml() throws IOException;

    void saveDataXmlJaxB() throws IOException, JAXBException;

    void loadDataYamlFasterXml() throws IOException;

    void saveDataYamlFasterXml();
}
