package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.service.IAuthService;
import com.t1.alieva.tm.api.service.IPropertyService;
import com.t1.alieva.tm.api.service.IUserService;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.LoginEmptyException;
import com.t1.alieva.tm.exception.field.PasswordEmptyException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.exception.user.AccessDeniedException;
import com.t1.alieva.tm.exception.user.PermissionException;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.util.HashUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import javax.naming.AuthenticationException;
import java.util.Arrays;

public class AuthService implements IAuthService {

    private final IPropertyService propertyService;

    private final IUserService userService;


    public AuthService(
            final IPropertyService propertyService,
            final IUserService userService) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    @Nullable
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            final String email) throws
            AbstractUserException,
            AbstractFieldException,
            AbstractEntityNotFoundException {
        return userService.create(login, password, email);
    }

    @Override
    @SneakyThrows
    public @NotNull User check(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.getLocked()) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AuthenticationException();
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return user;
    }
}
