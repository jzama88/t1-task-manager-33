package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.ITaskRepository;
import com.t1.alieva.tm.api.service.ITaskService;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.TaskNotFoundException;
import com.t1.alieva.tm.exception.field.*;
import com.t1.alieva.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository taskRepository) {

        super(taskRepository);
    }

    @Override
    @NotNull
    public Task create(
            @Nullable final String userId,
            @Nullable final String name) throws
            AbstractFieldException,
            AbstractEntityNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    @Nullable
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) throws
            AbstractFieldException,
            AbstractEntityNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }


    @Override
    @NotNull
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId) throws
            AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    @NotNull
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @NotNull final String description) throws
            AbstractFieldException,
            AbstractEntityNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @NotNull
    public Task updateByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @NotNull final String description) throws
            AbstractFieldException,
            AbstractEntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @Nullable
    public Task changeTaskStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @NotNull final Status status) throws
            AbstractFieldException,
            AbstractEntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) return null;
        @Nullable final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    @NotNull
    public Task changeTaskStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @NotNull final Status status) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new IndexIncorrectException();
        task.setStatus(status);
        return task;
    }
}
