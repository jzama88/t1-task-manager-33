package com.t1.alieva.tm.dto.request.user;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class UserUnlockRequest extends AbstractUserRequest
{
    @Nullable
    private String login;

    public UserUnlockRequest(@Nullable final String login) {
        this.login = login;
    }
}
