package com.t1.alieva.tm.dto.request.task;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskUnbindFromProjectRequest extends AbstractUserRequest {
    @Nullable
    private String projectId;

    @Nullable
    private String taskId;

    public TaskUnbindFromProjectRequest(@Nullable String projectId, @Nullable String taskId) {
        this.projectId = projectId;
        this.taskId = taskId;
    }
}
