package com.t1.alieva.tm.dto.request.project;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import com.t1.alieva.tm.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskChangeStatusByIndexRequest extends AbstractUserRequest {
    @Nullable
    private Integer index;

    @Nullable
    private Status status;

    public TaskChangeStatusByIndexRequest(@Nullable Integer index, @Nullable Status status) {
        this.index = index;
        this.status = status;
    }
}
