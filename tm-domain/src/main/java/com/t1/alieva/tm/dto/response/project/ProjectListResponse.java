package com.t1.alieva.tm.dto.response.project;

import com.t1.alieva.tm.dto.response.AbstractResponse;
import com.t1.alieva.tm.model.Project;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Getter
@Setter
public final class ProjectListResponse extends AbstractResponse {

    @Nullable
    @Getter
    private List<Project> projects;

    public ProjectListResponse(@Nullable List<Project> projects) {
        this.projects = projects;
    }
}
