package com.t1.alieva.tm.dto.response.user;

import com.t1.alieva.tm.dto.response.AbstractResultResponse;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class UserLoginResponse extends AbstractResultResponse {
    public UserLoginResponse(@NotNull Throwable throwable) {
        super(throwable);
    }
}
