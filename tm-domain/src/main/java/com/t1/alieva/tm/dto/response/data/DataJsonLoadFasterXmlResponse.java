package com.t1.alieva.tm.dto.response.data;

import com.t1.alieva.tm.dto.response.AbstractResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public final class DataJsonLoadFasterXmlResponse extends AbstractResponse
{
}
