package com.t1.alieva.tm.dto.request.user;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class UserUpdateProfileRequest extends AbstractUserRequest {

    @Nullable
    private String lastName;

    @Nullable
    private String firstName;

    @Nullable
    private String middleName;

    public UserUpdateProfileRequest(
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }
}
