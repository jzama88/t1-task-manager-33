package com.t1.alieva.tm.dto.request.user;

import com.t1.alieva.tm.dto.request.AbstractRequest;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractUserRequest extends AbstractRequest {

    @Nullable
    private String userId;
}
