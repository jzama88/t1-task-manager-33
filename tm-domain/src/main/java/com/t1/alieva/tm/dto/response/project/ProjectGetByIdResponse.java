package com.t1.alieva.tm.dto.response.project;

import com.t1.alieva.tm.dto.response.project.AbstractProjectResponse;
import com.t1.alieva.tm.model.Project;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectGetByIdResponse extends AbstractProjectResponse {
    public ProjectGetByIdResponse(@Nullable Project project) {
        super(project);
    }
}
