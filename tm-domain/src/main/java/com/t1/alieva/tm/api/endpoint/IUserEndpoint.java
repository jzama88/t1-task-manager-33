package com.t1.alieva.tm.api.endpoint;

import com.t1.alieva.tm.dto.request.user.*;
import com.t1.alieva.tm.dto.response.user.*;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import static com.t1.alieva.tm.api.endpoint.IEndpoint.*;

@WebService
public interface IUserEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME  + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserLockResponse lockUser(@WebParam(name = REQUEST, partName = REQUEST)
                              @NotNull UserLockRequest request);

    @NotNull
    @WebMethod
    UserUnlockResponse unlockUser(@WebParam(name = REQUEST, partName = REQUEST)
                                  @NotNull UserUnlockRequest request);

    @NotNull
    @WebMethod
    UserRemoveResponse removeUser(@WebParam(name = REQUEST, partName = REQUEST)
                                  @NotNull UserRemoveRequest request);

    @NotNull
    @WebMethod
    UserUpdateProfileResponse updateUserProfile(@WebParam(name = REQUEST, partName = REQUEST)
                                                @NotNull UserUpdateProfileRequest request);

    @NotNull
    @WebMethod
    UserChangePasswordResponse changeUserPassword(@WebParam(name = REQUEST, partName = REQUEST)
                                                  @NotNull UserChangePasswordRequest request);

    @NotNull
    @WebMethod
    UserRegistryResponse registryUser(@WebParam(name = REQUEST, partName = REQUEST)
                                      @NotNull UserRegistryRequest request);
}


