package com.t1.alieva.tm.dto.request.project;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCreateRequest extends AbstractUserRequest {
    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectCreateRequest
            (@Nullable String name,
             @Nullable String description) {
        this.name = name;
        this.description = description;
    }
}
