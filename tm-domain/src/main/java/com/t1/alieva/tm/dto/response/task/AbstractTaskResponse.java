package com.t1.alieva.tm.dto.response.task;

import com.t1.alieva.tm.dto.response.AbstractResponse;
import com.t1.alieva.tm.model.Task;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractTaskResponse extends AbstractResponse {
    @Nullable
    private Task task;

    public AbstractTaskResponse(@Nullable Task task) {
        this.task = task;
    }
}
