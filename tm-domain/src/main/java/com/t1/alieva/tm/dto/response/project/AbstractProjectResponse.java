package com.t1.alieva.tm.dto.response.project;

import com.t1.alieva.tm.dto.response.AbstractResponse;
import com.t1.alieva.tm.model.Project;
import lombok.Getter;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractProjectResponse extends AbstractResponse {

    @Getter
    private Project project;

    public AbstractProjectResponse(@Nullable Project project) {
        this.project = project;
    }
}
