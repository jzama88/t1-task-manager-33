package com.t1.alieva.tm.exception.entity;



public class ClassNotFoundException extends AbstractEntityNotFoundException {
    public ClassNotFoundException() {
        super("Error! Class not found...");
    }
}
