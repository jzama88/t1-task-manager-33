package com.t1.alieva.tm.command.data;

import com.t1.alieva.tm.dto.request.data.DataJsonSaveJaxBRequest;
import com.t1.alieva.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data in JSON file";

    @NotNull
    public static final String NAME = "data-save-json";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        serviceLocator.getDomainEndpoint().saveDataJsonJaxB(new DataJsonSaveJaxBRequest());
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}

