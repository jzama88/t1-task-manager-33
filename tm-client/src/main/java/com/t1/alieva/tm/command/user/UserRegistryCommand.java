package com.t1.alieva.tm.command.user;

import com.t1.alieva.tm.dto.request.user.UserRegistryRequest;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "User registry.";

    @NotNull
    public static final String NAME = "user-registry";

    @Override
    public void execute() throws
            AbstractUserException,
            AbstractFieldException,
            AbstractEntityNotFoundException, IOException, ClassNotFoundException {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(login, password, email);
        @Nullable final User user = getUserEndpoint().registryUser(request).getUser();
        showUser(user);
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }
}
