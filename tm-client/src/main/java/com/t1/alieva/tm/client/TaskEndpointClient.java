package com.t1.alieva.tm.client;

import com.t1.alieva.tm.api.endpoint.ITaskEndpointClient;
import com.t1.alieva.tm.dto.request.project.TaskChangeStatusByIndexRequest;
import com.t1.alieva.tm.dto.request.task.*;
import com.t1.alieva.tm.dto.request.user.UserLoginRequest;
import com.t1.alieva.tm.dto.request.user.UserLogoutRequest;
import com.t1.alieva.tm.dto.request.user.UserProfileRequest;
import com.t1.alieva.tm.dto.response.task.*;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

@NoArgsConstructor
public final class TaskEndpointClient extends AbstractEndpoint implements ITaskEndpointClient {

    public TaskEndpointClient(@NotNull AbstractEndpoint client) {
        super(client);
    }

    @Override
    @NotNull
    public TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskBindToProjectResponse.class);
    }

    @Override
    @NotNull
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

    @Override
    @NotNull
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @Override
    @NotNull
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @Override
    @NotNull
    public TaskClearResponse clearTask(@NotNull TaskClearRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskClearResponse.class);
    }

    @Override
    @NotNull
    public TaskCreateResponse createTask(@NotNull TaskCreateRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskCreateResponse.class);
    }

    @Override
    @NotNull
    public TaskGetByIdResponse getTaskById(@NotNull TaskGetByIdRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskGetByIdResponse.class);
    }

    @Override
    @NotNull
    public TaskGetByIndexResponse getTaskByIndex(@NotNull TaskGetByIndexRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskGetByIndexResponse.class);
    }

    @Override
    @NotNull
    public TaskListByProjectIdResponse getTaskByProjectId(@NotNull TaskListByProjectIdRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskListByProjectIdResponse.class);
    }

    @Override
    @NotNull
    public TaskListResponse listTask(@NotNull TaskListRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskListResponse.class);
    }

    @Override
    @NotNull
    public TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @Override
    @NotNull
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @Override
    @NotNull
    public TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @Override
    @NotNull
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request) throws IOException, ClassNotFoundException {
        return call(request, TaskUpdateByIndexResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("ADMIN", "ADMIN")));
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
        @NotNull final TaskEndpointClient taskEndpointClient = new TaskEndpointClient(authEndpointClient);
        System.out.println(taskEndpointClient.createTask(new TaskCreateRequest("Fourth Task", "Fourth Task description")));
        // System.out.println(taskEndpointClient.listTask(new TaskListRequest()).getTasks());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }
}
