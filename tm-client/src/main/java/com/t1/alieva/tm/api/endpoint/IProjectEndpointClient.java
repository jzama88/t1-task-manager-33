package com.t1.alieva.tm.api.endpoint;

import com.t1.alieva.tm.dto.request.project.*;
import com.t1.alieva.tm.dto.response.project.*;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public interface IProjectEndpointClient {

    @NotNull
    ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    ProjectClearResponse clearProject(@NotNull ProjectClearRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    ProjectGetByIdResponse getProjectById(@NotNull ProjectGetByIdRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    ProjectGetByIndexResponse getProjectByIndex(@NotNull ProjectGetByIndexRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    ProjectListResponse listProject(@NotNull ProjectListRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    ProjectUpdateByIdResponse updateProjectById(@NotNull ProjectUpdateByIdRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull ProjectUpdateByIndexRequest request) throws IOException, ClassNotFoundException;
}
