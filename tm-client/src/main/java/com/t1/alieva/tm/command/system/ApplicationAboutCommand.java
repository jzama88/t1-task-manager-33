package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.dto.request.system.ServerAboutRequest;
import com.t1.alieva.tm.dto.response.system.ServerAboutResponse;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    private static final String DESCRIPTION = "Show developer info.";

    @NotNull
    private static final String NAME = "about";

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[ABOUT]");
        @NotNull final ServerAboutResponse response = getServiceLocator().getSystemEndpoint().getAbout(new ServerAboutRequest());
        System.out.println("name: " + response.getName());
        System.out.println("e-mail: " + response.getEmail());
    }

    @Override
    public @NotNull String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
