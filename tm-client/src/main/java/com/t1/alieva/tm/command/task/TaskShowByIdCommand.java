package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.dto.request.task.TaskGetByIdRequest;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.Task;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return "t-show-by-id";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show Task by ID.";
    }

    @Override
    public void execute() throws
            AbstractFieldException,
            AbstractUserException, IOException, ClassNotFoundException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(id);
        @Nullable final Task task = getTaskEndpoint().getTaskById(request).getTask();
        showTask(task);
    }
}
