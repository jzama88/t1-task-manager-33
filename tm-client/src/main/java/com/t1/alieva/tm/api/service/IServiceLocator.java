package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.client.*;
import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    AuthEndpointClient getAuthEndpoint();

    @NotNull
    ProjectEndpointClient getProjectEndpoint();

    @NotNull
    SystemEndpointClient getSystemEndpoint();

    @NotNull
    DomainEndpointClient getDomainEndpoint();

    @NotNull
    TaskEndpointClient getTaskEndpoint();

    @NotNull
    UserEndpointClient getUserEndpoint();

    @NotNull
    ICommandService getCommandService();
}
