package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.dto.request.task.TaskRemoveByIndexRequest;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return "t-remove-by-index";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove Task by Index.";
    }

    @Override
    public void execute() throws
            AbstractFieldException,
            AbstractUserException, IOException, ClassNotFoundException {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(index);
        getTaskEndpoint().removeTaskByIndex(request);
    }
}
