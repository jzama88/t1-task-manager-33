package com.t1.alieva.tm.client;

import com.t1.alieva.tm.api.endpoint.IDomainEndpointClient;
import com.t1.alieva.tm.dto.request.data.*;
import com.t1.alieva.tm.dto.request.user.UserLoginRequest;
import com.t1.alieva.tm.dto.request.user.UserLogoutRequest;
import com.t1.alieva.tm.dto.response.data.*;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpoint implements IDomainEndpointClient {


    public DomainEndpointClient(@NotNull AbstractEndpoint client) {
        super(client);
    }

    @Override
    @NotNull
    public DataBackupLoadResponse loadDataBackup(@NotNull DataBackupLoadRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataBackupLoadResponse.class);
    }

    @Override
    @NotNull
    public DataBackupSaveResponse saveDataBackup(@NotNull DataBackupSaveRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataBackupSaveResponse.class);
    }

    @Override
    @NotNull
    public DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataBase64LoadResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @Override
    @NotNull
    public DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataBinaryLoadResponse.class);
    }

    @Override
    @NotNull
    public DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataBinarySaveResponse.class);
    }

    @Override
    @NotNull
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataJsonLoadFasterXmlResponse.class);
    }

    @Override
    @NotNull
    public DataJsonLoadJaxBResponse loadDataJsonJaxB(@NotNull DataJsonLoadJaxBRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataJsonLoadJaxBResponse.class);
    }

    @Override
    @NotNull
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataJsonSaveFasterXmlResponse.class);
    }

    @Override
    @NotNull
    public DataJsonSaveJaxBResponse saveDataJsonJaxB(@NotNull DataJsonSaveJaxBRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataJsonSaveJaxBResponse.class);
    }

    @Override
    @NotNull
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataXmlLoadFasterXmlResponse.class);
    }

    @Override
    @NotNull
    public DataXmlLoadJaxBResponse loadDataXmlJaxB(@NotNull DataXmlLoadJaxBRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataXmlLoadJaxBResponse.class);
    }

    @Override
    @NotNull
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataXmlSaveFasterXmlResponse.class);
    }

    @Override
    @NotNull
    public DataXmlSaveJaxBResponse saveDataXmlJaxB(@NotNull DataXmlSaveJaxBRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataXmlSaveJaxBResponse.class);
    }

    @Override
    @NotNull
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull DataYamlLoadFasterXmlRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataYamlLoadFasterXmlResponse.class);
    }

    @Override
    @NotNull
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull DataYamlSaveFasterXmlRequest request) throws IOException, ClassNotFoundException {
        return call(request, DataYamlSaveFasterXmlResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("admin", "admin")).getSuccess());
            @NotNull final DomainEndpointClient domainClient = new DomainEndpointClient(authEndpointClient);
            domainClient.saveDataBase64(new DataBase64SaveRequest());
        }
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
            @NotNull final DomainEndpointClient domainClient = new DomainEndpointClient(authEndpointClient);
            domainClient.saveDataBase64(new DataBase64SaveRequest());
        }
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }
}
