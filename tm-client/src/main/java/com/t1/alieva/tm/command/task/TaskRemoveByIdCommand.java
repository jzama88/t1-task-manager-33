package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.dto.request.task.TaskRemoveByIdRequest;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return "t-remove-by-id";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove Task by ID.";
    }

    @Override
    public void execute() throws
            AbstractFieldException,
            AbstractEntityNotFoundException,
            AbstractUserException, IOException, ClassNotFoundException {
        System.out.println("[REMOVE TASK BY ID");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(id);
        getTaskEndpoint().removeTaskById(request);
    }
}
