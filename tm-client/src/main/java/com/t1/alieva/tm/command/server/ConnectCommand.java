package com.t1.alieva.tm.command.server;

import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public final class ConnectCommand extends AbstractCommand {
    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getAuthEndpoint().connect();
        final Socket socket = getServiceLocator().getAuthEndpoint().getSocket();
        getServiceLocator().getProjectEndpoint().setSocket(socket);
        getServiceLocator().getTaskEndpoint().setSocket(socket);
        getServiceLocator().getDomainEndpoint().setSocket(socket);
        getServiceLocator().getSystemEndpoint().setSocket(socket);
        getServiceLocator().getUserEndpoint().setSocket(socket);

    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Connect to server";
    }

    @Override
    public @NotNull String getName() {
        return "connect";
    }

    @Override
    public Role[] getRoles() {
        return new Role[0];
    }
}
