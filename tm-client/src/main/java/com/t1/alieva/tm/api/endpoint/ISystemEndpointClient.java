package com.t1.alieva.tm.api.endpoint;

import com.t1.alieva.tm.dto.request.system.ServerAboutRequest;
import com.t1.alieva.tm.dto.request.system.ServerVersionRequest;
import com.t1.alieva.tm.dto.response.system.ServerAboutResponse;
import com.t1.alieva.tm.dto.response.system.ServerVersionResponse;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public interface ISystemEndpointClient {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) throws IOException, ClassNotFoundException;
}
