package com.t1.alieva.tm.command.user;

import com.t1.alieva.tm.dto.request.user.UserRemoveRequest;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class UserRemoveCommand extends AbstractUserCommand {

    @Override
    public void execute() throws
            AbstractUserException,
            AbstractFieldException,
            AbstractEntityNotFoundException, IOException, ClassNotFoundException {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(login);
        getUserEndpoint().removeUser(request);
    }

    @Override
    @NotNull
    public String getDescription() {
        return "user remove";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-remove";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]
                {
                        Role.ADMIN
                };
    }
}
