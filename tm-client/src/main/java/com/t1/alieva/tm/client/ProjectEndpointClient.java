package com.t1.alieva.tm.client;

import com.t1.alieva.tm.api.endpoint.IProjectEndpointClient;
import com.t1.alieva.tm.dto.request.project.*;
import com.t1.alieva.tm.dto.request.user.UserLoginRequest;
import com.t1.alieva.tm.dto.request.user.UserLogoutRequest;
import com.t1.alieva.tm.dto.request.user.UserProfileRequest;
import com.t1.alieva.tm.dto.response.project.*;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

@NoArgsConstructor
public final class ProjectEndpointClient extends AbstractEndpoint implements IProjectEndpointClient {

    public ProjectEndpointClient(@NotNull AbstractEndpoint client) {
        super(client);
    }

    @Override
    @NotNull
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request) throws IOException, ClassNotFoundException {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @Override
    @NotNull
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request) throws IOException, ClassNotFoundException {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @Override
    @NotNull
    public ProjectClearResponse clearProject(@NotNull ProjectClearRequest request) throws IOException, ClassNotFoundException {
        return call(request, ProjectClearResponse.class);
    }

    @Override
    @NotNull
    public ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request) throws IOException, ClassNotFoundException {
        return call(request, ProjectCreateResponse.class);
    }

    @Override
    @NotNull
    public ProjectGetByIdResponse getProjectById(@NotNull ProjectGetByIdRequest request) throws IOException, ClassNotFoundException {
        return call(request, ProjectGetByIdResponse.class);
    }

    @Override
    @NotNull
    public ProjectGetByIndexResponse getProjectByIndex(@NotNull ProjectGetByIndexRequest request) throws IOException, ClassNotFoundException {
        return call(request, ProjectGetByIndexResponse.class);
    }

    @Override
    @NotNull
    public ProjectListResponse listProject(@NotNull ProjectListRequest request) throws IOException, ClassNotFoundException {
        return call(request, ProjectListResponse.class);
    }

    @Override
    @NotNull
    public ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request) throws IOException, ClassNotFoundException {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @Override
    @NotNull
    public ProjectUpdateByIdResponse updateProjectById(@NotNull ProjectUpdateByIdRequest request) throws IOException, ClassNotFoundException {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @Override
    @NotNull
    public ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull ProjectUpdateByIndexRequest request) throws IOException, ClassNotFoundException {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();

        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());

        @NotNull final ProjectEndpointClient projectClient = new ProjectEndpointClient(authEndpointClient);
        System.out.println(projectClient.createProject(new ProjectCreateRequest("HELLO", "WORLD")));
        // System.out.println(projectClient.listProject(new ProjectListRequest()).getProjects());

        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }
}
