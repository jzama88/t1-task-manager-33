package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.dto.request.project.ProjectGetByIdRequest;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return "p-show-by-id";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show Project by ID.";
    }

    @Override
    public void execute() throws AbstractFieldException, AbstractUserException, IOException, ClassNotFoundException {
        System.out.println(("[SHOW PROJECT BY ID]"));
        System.out.println(("[ENTER ID]"));
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(id);
        @Nullable final Project project = getProjectEndpoint().getProjectById(request).getProject();
        showProject(project);
    }
}
