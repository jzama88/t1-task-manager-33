package com.t1.alieva.tm.client;

import com.t1.alieva.tm.api.endpoint.ISystemEndpointClient;
import com.t1.alieva.tm.dto.request.system.ServerAboutRequest;
import com.t1.alieva.tm.dto.request.system.ServerVersionRequest;
import com.t1.alieva.tm.dto.response.system.ServerAboutResponse;
import com.t1.alieva.tm.dto.response.system.ServerVersionResponse;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpointClient {

    @Override
    @NotNull
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) throws IOException, ClassNotFoundException {
        return call(request, ServerAboutResponse.class);
    }

    @Override
    @NotNull
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) throws IOException, ClassNotFoundException {
        return call(request, ServerVersionResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());

        @NotNull final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());

        client.disconnect();
    }
}
